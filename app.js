const Express = require('express');
const app = Express();
const bodyParser = require('body-parser');
const route = require('./routes/route');

app.use(bodyParser.json());
app.use('/', route);

app.listen(8083, 'localhost', function (err) {
    if(err) {
        console.log("Connection failed");
        process.exit(-1);
    }
    console.log("Listening on port 8083");
})
